// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FightclashCharacter.h"
#include "FightclashProjectile.h"
#include "FCCharacterMovement.h"
#include "Animation/AnimInstance.h"
#include "FCGameState.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "Net/UnrealNetwork.h"
#include "Weapon.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId
#include "Engine.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AFightclashCharacter

AFightclashCharacter::AFightclashCharacter(const FObjectInitializer& ObjectInitializer)
    : Super(ObjectInitializer.SetDefaultSubobjectClass<UFCCharacterMovement>(ACharacter::CharacterMovementComponentName))
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

    PlayerTeam = 0;
    bIsJumping = false;
    MaxHealth = 100.f;

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->bSelfShadowOnly = true;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

    GripSocketName1P = FName("GripPoint");
    GripSocketName3P = FName("GripPoint");

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void AFightclashCharacter::Destroyed() {
    Super::Destroyed();
    if (Role == ROLE_Authority) {
        for (auto Weapon : Inventory) {
            Weapon->Destroy();
        }
    }
}

void AFightclashCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AFightclashCharacter, Health);
    DOREPLIFETIME(AFightclashCharacter, CurrentWeapon);
    DOREPLIFETIME(AFightclashCharacter, bIsJumping);
    DOREPLIFETIME(AFightclashCharacter, AimOffset);
    DOREPLIFETIME(AFightclashCharacter, Inventory);
}

void AFightclashCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();
    Health = MaxHealth;

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
}

void AFightclashCharacter::EndPlay(const EEndPlayReason::Type EndPlayReason) {
    Super::EndPlay(EndPlayReason);
    
    if (CurrentWeapon != nullptr) {
        CurrentWeapon->UnequipFromOwner();
    }
}

void AFightclashCharacter::Tick(float DeltaSeconds) {
    Super::Tick(DeltaSeconds);

    if (Role == ROLE_Authority) {
        AimOffset = GetControlRotation() - GetActorRotation();
    }
}

void AFightclashCharacter::OnRep_Health() {
    OnHealthUpdated.Broadcast();
}

//////////////////////////////////////////////////////////////////////////
// Inventory

void AFightclashCharacter::OnRep_CurrentWeapon(AWeapon* LastWeapon) {
    SetCurrentWeapon(CurrentWeapon, LastWeapon);
}
void AFightclashCharacter::OnRep_Inventory(TArray<AWeapon*> PreviousInventory) {
    // O(n^2), definitely inefficient but this isn't performance-sensitive
    // Check for new items
    for (int32 CurrentInventoryIndex = 0; CurrentInventoryIndex < Inventory.Num(); CurrentInventoryIndex += 1) {
        if (!PreviousInventory.Find(Inventory[CurrentInventoryIndex])) {
            OnItemAdded.Broadcast(CurrentInventoryIndex, Inventory[CurrentInventoryIndex]);
        }
    }
    // Check for removed items
    for (int32 PreviousInventoryIndex = 0; PreviousInventoryIndex < PreviousInventory.Num(); PreviousInventoryIndex += 1) {
        if (!Inventory.Find(PreviousInventory[PreviousInventoryIndex])) {
            OnItemRemoved.Broadcast(PreviousInventoryIndex, PreviousInventory[PreviousInventoryIndex]);
        }
    }
    OnInventoryChanged();
}

void AFightclashCharacter::EquipWeapon(AWeapon* Weapon) {
    UE_LOG(LogTemp, Warning, TEXT("Character %s equipping weapon %s"), *GetName(), *Weapon->GetName());
    if (Role < ROLE_Authority) {
        ServerEquipWeapon(Weapon);
    }
    else {
        SetCurrentWeapon(Weapon, CurrentWeapon);
    }
    if (GetController() != nullptr && GetController()->IsLocalPlayerController()) {
        int32 EquippedWeaponIndex;
        Inventory.Find(Weapon, EquippedWeaponIndex);
        OnItemEquipped.Broadcast(EquippedWeaponIndex, Weapon);
    }
}

void AFightclashCharacter::EquipNextWeapon() {
    UE_LOG(LogTemp, Display, TEXT("Character %s cycling to next weapon..."), *GetName());
    if (Inventory.Num() == 0) {
        return;
    }
    int32 EquippedWeaponIndex = Inventory.Find(CurrentWeapon);
    int32 NextWeaponIndex = EquippedWeaponIndex == INDEX_NONE ? 0 : (EquippedWeaponIndex + 1) % Inventory.Num();
    EquipWeapon(Inventory[NextWeaponIndex]);
}

void AFightclashCharacter::EquipPreviousWeapon() {
    UE_LOG(LogTemp, Display, TEXT("Character %s cycling to previous weapon..."), *GetName());
    if (Inventory.Num() == 0) {
        return;
    }
    int32 EquippedWeaponIndex = Inventory.Find(CurrentWeapon);
    int32 PreviousWeaponIndex = EquippedWeaponIndex == INDEX_NONE ? 0 : (EquippedWeaponIndex + Inventory.Num() - 1) % Inventory.Num();
    EquipWeapon(Inventory[PreviousWeaponIndex]);
}

void AFightclashCharacter::ServerEquipWeapon_Implementation(AWeapon* Weapon) {
    SetCurrentWeapon(Weapon, CurrentWeapon);
}

bool AFightclashCharacter::ServerEquipWeapon_Validate(AWeapon* Weapon) {
    return true;
}

void AFightclashCharacter::AddWeaponToInventory(AWeapon* Weapon) {
    if (Role < ROLE_Authority) {
        ServerAddWeaponToInventory(Weapon);
        return;
    }
    UE_LOG(LogTemp, Display, TEXT("Character %s adding weapon %s added to its inventory."), *GetNameSafe(this), *GetNameSafe(Weapon));
    Weapon->SetNewOwner(this);
    int32 NewWeaponIndex = Inventory.Add(Weapon);
    OnItemAdded.Broadcast(NewWeaponIndex, Weapon);
    OnInventoryChanged();
}

void AFightclashCharacter::ServerAddWeaponToInventory_Implementation(AWeapon* Weapon) {
    AddWeaponToInventory(Weapon);
}

bool AFightclashCharacter::ServerAddWeaponToInventory_Validate(AWeapon* Weapon) {
    return true;
}

void AFightclashCharacter::SetCurrentWeapon(AWeapon* NewWeapon, AWeapon* LastWeapon) {
    if (NewWeapon == LastWeapon) {
        return;
    }

    if (LastWeapon != nullptr) {
        LastWeapon->UnequipFromOwner();
    }

    CurrentWeapon = NewWeapon;

    if (NewWeapon != nullptr) {
        NewWeapon->EquipToCharacter(this);
    }
}

//////////////////////////////////////////////////////////////////////////
// Combat

float AFightclashCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
    if (Role < ROLE_Authority) {
        ServerTakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
        return DamageAmount;
    }

    // If friendly fire isn't enabled 
    if (EventInstigator != nullptr && EventInstigator->GetPawn() != nullptr) {
        AFightclashCharacter* OtherCharacter = Cast<AFightclashCharacter>(EventInstigator->GetPawn());
        if (OtherCharacter != nullptr && OtherCharacter->PlayerTeam == PlayerTeam) {
            UWorld* World = GetWorld();
            if (World != nullptr) {
                AFCGameState* GameState = Cast<AFCGameState>(World->GetGameState());
                if (GameState != nullptr && !GameState->bFriendlyFireEnabled) {
                    return 0.f;
                }
            }
        }
    }

    float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

    if (DamageEvent.GetTypeID() == FPointDamageEvent::ClassID) {
        FPointDamageEvent PointDamage = *((FPointDamageEvent*)&DamageEvent);
        if (PointDamage.HitInfo.Component == GetMesh()) {
            if (BodyDamageModifiers.Contains(PointDamage.HitInfo.BoneName)) {
                ActualDamage *= *BodyDamageModifiers.Find(PointDamage.HitInfo.BoneName);
            }
        }
    }

    Health -= ActualDamage;
    OnHealthUpdated.Broadcast();

    if (Health <= 0.f) {
        OnKilledDelegate.Broadcast(this, EventInstigator);
        Destroy();
    }
    
    return ActualDamage;
}

void AFightclashCharacter::ServerTakeDamage_Implementation(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
    TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

bool AFightclashCharacter::ServerTakeDamage_Validate(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) {
    return true;
}

void AFightclashCharacter::StartFiring() {
    if (!bWantsToFire) {
        bWantsToFire = true;
        if (CurrentWeapon != nullptr) {
            CurrentWeapon->StartFire();
        }
    }
}

void AFightclashCharacter::StopFiring() {
    if (bWantsToFire) {
        bWantsToFire = false;
        if (CurrentWeapon != nullptr) {
            CurrentWeapon->StopFire();
        }
    }
}

void AFightclashCharacter::ServerSetJump_Implementation(bool Jumping) {
    bIsJumping = Jumping;
}

bool AFightclashCharacter::ServerSetJump_Validate(bool Jumping) {
    return true;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AFightclashCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind special movement events
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AFightclashCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &AFightclashCharacter::StopJumping);

    PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AFightclashCharacter::StartSprinting);
    PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AFightclashCharacter::StopSprinting);

    PlayerInputComponent->BindAction("Dodge", IE_Pressed, this, &AFightclashCharacter::PerformDodge);

	// Bind fire event
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AFightclashCharacter::StartFiring);
    PlayerInputComponent->BindAction("Fire", IE_Released, this, &AFightclashCharacter::StopFiring);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AFightclashCharacter::OnResetVR);

    // Bind item events
    PlayerInputComponent->BindAction("NextWeapon", IE_Pressed, this, &AFightclashCharacter::EquipNextWeapon);
    PlayerInputComponent->BindAction("PreviousWeapon", IE_Pressed, this, &AFightclashCharacter::EquipPreviousWeapon);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &AFightclashCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AFightclashCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AFightclashCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AFightclashCharacter::LookUpAtRate);
}

void AFightclashCharacter::PerformDodge() {
    UFCCharacterMovement* FCCharacterMovement = Cast<UFCCharacterMovement>(GetCharacterMovement());
    if (FCCharacterMovement != nullptr) {
        FCCharacterMovement->PerformDodgeInMoveDirection();
    }
}

void AFightclashCharacter::Jump() {
    Super::Jump();
    if (Role < ROLE_Authority) {
        ServerSetJump(true);
    }
    else {
        bIsJumping = true;
    }
}

void AFightclashCharacter::StopJumping() {
    if (Role < ROLE_Authority) {
        ServerSetJump(false);
    }
    else {
        bIsJumping = false;
    }
}

void AFightclashCharacter::StartSprinting() {
    UFCCharacterMovement* CharacterMovement = Cast<UFCCharacterMovement>(GetCharacterMovement());
    if (CharacterMovement != nullptr) {
        CharacterMovement->SetSprinting(true);
    }
}

void AFightclashCharacter::StopSprinting() {
    UFCCharacterMovement* CharacterMovement = Cast<UFCCharacterMovement>(GetCharacterMovement());
    if (CharacterMovement != nullptr) {
        CharacterMovement->SetSprinting(false);
    }
}

void AFightclashCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AFightclashCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		StartFiring();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AFightclashCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
    StopFiring();
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

void AFightclashCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
	{
		if (TouchItem.bIsPressed)
		{
			if (GetWorld() != nullptr)
			{
				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
				if (ViewportClient != nullptr)
				{
					FVector MoveDelta = Location - TouchItem.Location;
					FVector2D ScreenSize;
					ViewportClient->GetViewportSize(ScreenSize);
					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
					{
						TouchItem.bMoved = true;
						float Value = ScaledDelta.X * BaseTurnRate;
						AddControllerYawInput(Value);
					}
					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
					{
						TouchItem.bMoved = true;
						float Value = ScaledDelta.Y * BaseTurnRate;
						AddControllerPitchInput(Value);
					}
					TouchItem.Location = Location;
				}
				TouchItem.Location = Location;
			}
		}
	}
}

bool AFightclashCharacter::CanJumpInternal_Implementation() const {
    bool bCanJump = Super::CanJumpInternal_Implementation();

    UFCCharacterMovement* FCCharacterMovement = Cast<UFCCharacterMovement>(GetCharacterMovement());
    if (!bCanJump && FCCharacterMovement != nullptr) {
        bCanJump = FCCharacterMovement->CanJump();
    }

    return bCanJump;
}

void AFightclashCharacter::MoveForward(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void AFightclashCharacter::MoveRight(float Value)
{
	if (Controller != nullptr && Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void AFightclashCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AFightclashCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AFightclashCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AFightclashCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AFightclashCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AFightclashCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}
