// Fill out your copyright notice in the Description page of Project Settings.

#include "FCGameState.h"

AFCGameState::AFCGameState() {
    bFriendlyFireEnabled = true;
}

void AFCGameState::HandleMatchHasEnded() {
    Super::HandleMatchHasEnded();
    OnMatchEnded();
}
