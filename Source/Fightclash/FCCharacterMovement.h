// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "FCCharacterMovement.generated.h"

UCLASS()
class FIGHTCLASH_API UFCCharacterMovement : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
    UFCCharacterMovement();

    UPROPERTY(EditAnywhere, Category = "Sprint")
    float SprintSpeedMultiplier;
    UPROPERTY(EditAnywhere, Category = "Sprint")
    float SprintAccelerationMultiplier;

    UPROPERTY(EditAnywhere, Category = "Dodge")
    float DodgeStrength;
    UPROPERTY(EditAnywhere, Category = "Dodge")
    float GroundDodgeStrengthMultiplier;

    UPROPERTY(EditAnywhere, Category = "Dodge")
    float DodgeJumpSpeedThreshold;

    ///@brief Flag for activating sprint.
    uint8 bWantsToSprint : 1;
    ///@brief Flag for activating dodge.
    uint8 bWantsToDodge : 1;
    FVector DodgeDirection;

    ///@brief Activate or deactivate sprint.
    void SetSprinting(bool bShouldSprint);

    ///@brief Activate a dodge.
    void PerformDodgeInMoveDirection();
    void PerformDodge(FVector DodgeDir);

    virtual float GetMaxSpeed() const override;
    virtual float GetMaxAcceleration() const override;

    virtual void OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity) override;
    virtual FVector NewFallVelocity(const FVector& InitialVelocity, const FVector& Gravity, float DeltaTime) const override;

    bool IsMovingForward() const;

//////////////////////////////////////////////////////////////////////////
// Jumping

    virtual bool DoJump(bool bReplayingMoves) override;
    virtual void ProcessLanded(const FHitResult& Hit, float RemainingTime, int32 Iterations) override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jump")
    int32 MaxJumpCount;
    UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = "Jump")
    int32 JumpCount;

    bool CanJump() const;

//////////////////////////////////////////////////////////////////////////
// Wallslide

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Wallslide")
    float WallslideStrength;

//////////////////////////////////////////////////////////////////////////
// Replication

public:
    UFUNCTION(Server, Reliable, WithValidation)
    void ServerPerformDodge(const FVector& DodgeDir);
    void ServerPerformDodge_Implementation(const FVector& DodgeDir);
    bool ServerPerformDodge_Validate(const FVector& DodgeDir);

    virtual class FNetworkPredictionData_Client* GetPredictionData_Client() const override;

    virtual void UpdateFromCompressedFlags(uint8 Flags) override;

public:
    static const FSavedMove_Character::CompressedFlags SPRINT_FLAG = FSavedMove_Character::FLAG_Custom_0;
    static const FSavedMove_Character::CompressedFlags DODGE_FLAG = FSavedMove_Character::FLAG_Custom_1;
};

class FSavedMove_FCCharacter : public FSavedMove_Character {
    virtual void Clear() override;

    virtual uint8 GetCompressedFlags() const override;

    virtual bool CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const override;

    virtual void SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, class FNetworkPredictionData_Client_Character & ClientData) override;
    virtual void PrepMoveFor(class ACharacter* Character) override;

    FVector DodgeDirection;
    int32 JumpCount;
    uint8 bWantsToSprint : 1;
    uint8 bWantsToDodge : 1;
};

class FNetworkPredictionData_Client_FCCharacter : public FNetworkPredictionData_Client_Character
{
public:
    FNetworkPredictionData_Client_FCCharacter(const UCharacterMovementComponent& ClientMovement);

    ///@brief Allocates a new copy of our custom saved move
    virtual FSavedMovePtr AllocateNewMove() override;
};
