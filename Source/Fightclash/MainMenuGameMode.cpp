// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenuGameMode.h"
#include "MenuController.h"
#include "FCGameInstance.h"
#include "GameSettings.h"

AMainMenuGameMode::AMainMenuGameMode() {
    PlayerControllerClass = AMenuController::StaticClass();
}

void AMainMenuGameMode::BeginPlay() {
    Super::BeginPlay();
    UFCGameInstance* GameInstance = Cast<UFCGameInstance>(GetGameInstance());
    if (GameInstance != nullptr) {
        UGameSettings* GameSettings = GameInstance->GetCurrentGameSettings();
        GameSettings->ApplyCurrentConfig();
    }
}
