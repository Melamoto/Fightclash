// Fill out your copyright notice in the Description page of Project Settings.

#include "WeaponPickup.h"
#include "FightclashCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "Weapon.h"

AWeaponPickup::AWeaponPickup() {
    WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
    WeaponMesh->SetCollisionObjectType(ECC_WorldDynamic);
    WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    WeaponMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
    WeaponMesh->SetIsReplicated(true);
    WeaponMesh->SetupAttachment(RootComponent);
}

void AWeaponPickup::PostInitializeComponents() {
    Super::PostInitializeComponents();
    if (WeaponMesh->SkeletalMesh != nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("WeaponPickup (%s) should not have WeaponMesh set in its Constructor."), *GetName());
        return;
    }
    UpdateWeaponMesh();
}

#if WITH_EDITOR
void AWeaponPickup::PostEditChangeProperty(struct FPropertyChangedEvent & PropertyChangedEvent) {
    Super::PostEditChangeProperty(PropertyChangedEvent);
    if (PropertyChangedEvent.GetPropertyName() == GET_MEMBER_NAME_CHECKED(AWeaponPickup, WeaponClass)) {
        UpdateWeaponMesh();
    }
}
#endif

void AWeaponPickup::OnPickedUp(AFightclashCharacter* Character) {
    UWorld* World = GetWorld();
    check(World);
    FActorSpawnParameters ActorSpawnParameters;
    AWeapon* Weapon = World->SpawnActor<AWeapon>(WeaponClass);
    Character->AddWeaponToInventory(Weapon);
}

void AWeaponPickup::UpdateWeaponMesh() {
    AWeapon* DefaultWeapon = WeaponClass == nullptr ? nullptr : WeaponClass->GetDefaultObject<AWeapon>();
    if (DefaultWeapon == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("WeaponPickup (%s) has no valid default weapon class, failed to spawn weapon mesh."), *GetName());
        return;
    }
    WeaponMesh->SetSkeletalMesh(DefaultWeapon->Mesh3P->SkeletalMesh);
}