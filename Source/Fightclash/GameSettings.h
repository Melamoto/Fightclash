// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameFramework/PlayerInput.h"
#include "GameSettings.generated.h"

USTRUCT(BlueprintType)
struct FInputBindingResult {
    GENERATED_BODY()

    FInputBindingResult() : InputName(""), bIsAxis(false), AxisScale(0.f) {}
    FInputBindingResult(FName InputName) : InputName(InputName), bIsAxis(false), AxisScale(0.f) {}
    FInputBindingResult(FName InputName, float AxisScale) : InputName(InputName), bIsAxis(true), AxisScale(AxisScale) {}

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    FName InputName;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    bool bIsAxis;

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
    float AxisScale;

};

/**
 * 
 */
UCLASS(Config=Game, BlueprintType)
class FIGHTCLASH_API UGameSettings : public UObject
{
	GENERATED_BODY()
	
public:
    UGameSettings();

    // Graphics Settings
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 ViewDistanceQuality;
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 AntiAliasingQuality;
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 ShadowQuality;
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 PostProcessQuality;
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 TextureQuality;
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 EffectsQuality;
    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Graphics Settings")
    int32 FoliageQuality;

    // Screen Settings
    UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = "Screen Settings")
    int32 ResWidth;
    UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = "Screen Settings")
    int32 ResHeight;
    UPROPERTY(BlueprintReadWrite, VisibleInstanceOnly, Category = "Screen Settings")
    TEnumAsByte<EWindowMode::Type> FullscreenMode;
	
    // Audio Settings
    UPROPERTY(Config, BlueprintReadWrite, EditInstanceOnly, Category = "Audio Settings", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
    float MasterVolume;
    UPROPERTY(Config, BlueprintReadWrite, EditInstanceOnly, Category = "Audio Settings", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
    float MusicVolume;
    UPROPERTY(Config, BlueprintReadWrite, EditInstanceOnly, Category = "Audio Settings", meta = (ClampMin = "0.0", ClampMax = "1.0", UIMin = "0.0", UIMax = "1.0"))
    float EffectsVolume;

    // Input Settings
    UPROPERTY(Config, BlueprintReadOnly, VisibleInstanceOnly, Category = "Input Settings")
    bool bUserHasCustomMappings = false;
    UPROPERTY(Config, BlueprintReadOnly, VisibleInstanceOnly, Category = "Input Settings")
    TArray<FInputActionKeyMapping> ActionMappings;
    UPROPERTY(Config, BlueprintReadOnly, VisibleInstanceOnly, Category = "Input Settings")
    TArray<FInputAxisKeyMapping> AxisMappings;

public:
    UFUNCTION(BlueprintCallable, Category = "Settings")
    void LoadFullConfig();
    UFUNCTION(BlueprintCallable, Category = "Settings")
    void SaveFullConfig();
    UFUNCTION(BlueprintCallable, Category = "Settings")
    void ChangeScreenResolution(const int32 NewWidth, const int32 NewHeight, const EWindowMode::Type NewFullscreenMode);
    UFUNCTION(BlueprintCallable, Category = "Settings")
    void ConfirmCurrentScreenResolution(bool IsAcceptable);
    UFUNCTION(BlueprintCallable, Category = "Settings")
    void ApplyCurrentConfig();

    UFUNCTION(BlueprintCallable, Category = "Input Settings")
    bool AddNewActionMapping(FInputActionKeyMapping NewMapping, uint8 Position, TArray<FInputBindingResult>& RemovedBindings);
    UFUNCTION(BlueprintCallable, Category = "Input Settings")
    bool AddNewAxisMapping(FInputAxisKeyMapping NewMapping, uint8 Position, TArray<FInputBindingResult>& RemovedBindings);
    UFUNCTION(BlueprintCallable, Category = "Input Settings")
    void ResetKeybindingsToDefaults();
    
    void ApplySettingsToPlayerInput(class UPlayerInput* PlayerInput);

private:
    class UGameUserSettings* GetGameUserSettings();

    class USoundMix* MasterSoundMix;
    class USoundClass* MusicSoundClass;
    class USoundClass* EffectsSoundClass;

    void ApplyInputOverrideMappings();

    bool RemoveActionMappingForInputChord(FKey Key, bool bShift, bool bCtrl, bool bAlt, bool bCmd, TArray<FInputBindingResult>& RemovedBindings);
    bool RemoveActionMappingForKey(FKey Key, TArray<FInputBindingResult>& RemovedBindings);
    bool RemoveAxisMappingForKey(FKey Key, TArray<FInputBindingResult>& RemovedBindings);

    int32 InsertActionKeyMapping(FInputActionKeyMapping NewMapping, uint8 Position);
    int32 InsertAxisKeyMapping(FInputAxisKeyMapping NewMapping, uint8 Position);

    // Standard comparisons, returns n < 0 if A < B, n == 0 if A == B, and 0 < n if A > B
    static int32 CompareActionMappingOrder(FInputActionKeyMapping A, FInputActionKeyMapping B);
    static int32 CompareAxisMappingOrder(FInputAxisKeyMapping A, FInputAxisKeyMapping B);

    bool KeysHaveSameInputMode(FKey A, FKey B);

};
