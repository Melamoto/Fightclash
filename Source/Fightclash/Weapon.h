// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"



UENUM(BlueprintType)
enum class EWeaponState : uint8 {
    Idle,
    Firing,
    Reloading,
    Equipping
};

/**
 * 
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class FIGHTCLASH_API AWeapon : public AActor
{
	GENERATED_BODY()

public:
    AWeapon();

    virtual void Destroyed();

public:

    // Debugging code
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Debug")
    bool bShowDebugData;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
    class AFightclashCharacter* FCOwner;

    class AFCPlayerController* GetOwningPlayer() const;

    // Mesh

    // Weapon mesh: 1st person view (seen only by owner)
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
    class USkeletalMeshComponent* Mesh1P;

    // Weapon mesh: 3rd person view (seen only by others)
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
    class USkeletalMeshComponent* Mesh3P;

    /** Name of socket on gun mesh where projectiles should spawn. */
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
    FName MuzzleSocketName;

    USkeletalMeshComponent* GetWeaponMesh() const;

    // Inventory
    UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "Gameplay")
    bool bIsEquipped;

    void SetNewOwner(class AFightclashCharacter* Owner);

    UFUNCTION(BlueprintCallable)
    void EquipToCharacter(class AFightclashCharacter* EquippingCharacter);

    UFUNCTION(BlueprintCallable)
    void UnequipFromOwner();

    //Input
    void StartFire();
    UFUNCTION(Server, Reliable, WithValidation)
    void ServerStartFire();

    void StopFire();
    UFUNCTION(Server, Reliable, WithValidation)
    void ServerStopFire();

    // Weapon state and transitions
    UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "Gameplay|Weapon State")
    EWeaponState CurrentState;

    void UpdateWeaponState();
    void SetWeaponState(EWeaponState NewState);

    UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "Gameplay|Weapon State")
    bool bWantsToFire;

    // Weapon usage
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Weapon Properties")
    float TimeBetweenShots;

    float LastTimeFired;

    FTimerHandle TimerHandle_FireWeapon;

    void FireWeapon();

    /**
     * [server] Simulates firing a single shot from the weapon
     */
    virtual void FireSingleShot() PURE_VIRTUAL(AWeapon::FireSingleShot,);
    /**
    * [client] Applies any player-relevant effects from firing a weapon, such as increasing Spread and Recoil (occurs after FireSingleShot).
    */
    virtual void FireSingleShotLocal() {}

    virtual void GetShotOriginAndDirection(FVector& Origin, FVector& Direction) const;
    virtual FVector GetAimDirection() const;
    virtual FVector GetMuzzleLocation() const;

    // [server] play sounds and animations when firing, replicates to clients
    UFUNCTION(NetMulticast, Unreliable)
    void FireWeaponEffects();

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Cosmetic")
    class UAnimMontage* FPFireAnimation;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Cosmetic")
    class UAnimMontage* TPFireAnimation;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Cosmetic")
    class USoundBase* FireSound;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Cosmetic")
    FName WeaponName;
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Cosmetic")
    class UTexture2D* WeaponThumbnail;
};
