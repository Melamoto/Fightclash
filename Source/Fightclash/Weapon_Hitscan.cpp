// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon_Hitscan.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"
#include "FightclashCharacter.h"
#include "Net/UnrealNetwork.h"
#include "Components/DecalComponent.h"

AWeapon_Hitscan::AWeapon_Hitscan() {
    MaxRange = 50000.f;
    Damage = 5.f;
    TimeBetweenShots = 0.1f;
    
    MaxSpread = 3.f;
    SpreadGrowthPerShot = 0.5f;
    SpreadDecayPerSecond = 5.f;

    MaxRecoil = FVector2D(5.f, 10.f);
    RecoilPerShot = FVector2D(0.5f, 1.f);
    RecoilRecoveryPerSecond = 30.f;

    CurrentSpread = 0.f;
    CurrentRecoil = FVector2D(0.f, 0.f);

    BulletDecalSize = FVector(1.f, 5.f, 5.f);
    BulletDecalDuration = 5.f;
    BulletDecalFadeTime = 2.f;
}

void AWeapon_Hitscan::Tick(float DeltaSeconds) {
    Super::Tick(DeltaSeconds);

    if (CurrentSpread > 0.f) {
        CurrentSpread = FMath::Max(0.f, CurrentSpread - (SpreadDecayPerSecond * DeltaSeconds));
    }

    if (CurrentState != EWeaponState::Firing && !CurrentRecoil.IsNearlyZero()) {
        float CurrentRecoilSize = CurrentRecoil.Size();
        float RecoilRecovery = DeltaSeconds * RecoilRecoveryPerSecond;
        if (CurrentRecoilSize < RecoilRecovery) {
            SetRecoil(FVector2D::ZeroVector);
        }
        else {
            SetRecoil(CurrentRecoil * (CurrentRecoilSize - RecoilRecovery) / CurrentRecoilSize);
        }

    }
}

void AWeapon_Hitscan::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const {
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AWeapon_Hitscan, CurrentSpread);
}

void AWeapon_Hitscan::GetShotOriginAndDirection(FVector& Origin, FVector& Direction) const {
    AController* InstigatorController = GetInstigatorController();
    if (InstigatorController == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("Weapon %s attempted to fire with no instigator controller, instigator is %s"), *GetNameSafe(this), *GetNameSafe(Instigator));
        return;
    }
    FRotator DirectionRotator;
    InstigatorController->GetPlayerViewPoint(Origin, DirectionRotator);
    Origin = FCOwner->GetFirstPersonCameraComponent()->GetComponentLocation();
    Direction = DirectionRotator.Vector();
}

void AWeapon_Hitscan::FireSingleShotLocal() {
    AddRecoilAndSpreadForSingleShot();
}

void AWeapon_Hitscan::FireSingleShot() {
    FVector FireDirection;
    FVector FireOrigin;
    GetShotOriginAndDirection(FireOrigin, FireDirection);

    // Don't use RandomSeed for calculations that aren't also performed on the client
    FireDirection = FMath::VRandCone(FireDirection, FMath::DegreesToRadians(CurrentSpread));
    FVector FireEnd = FireOrigin + FireDirection * MaxRange;

    UWorld* World = GetWorld();
    check(World);

    FCollisionQueryParams CollisionQueryParams;
    CollisionQueryParams.AddIgnoredActor(Instigator);
    CollisionQueryParams.AddIgnoredActor(this);

    FHitResult HitResult;
    bool AnyHits = World->LineTraceSingleByChannel(HitResult, FireOrigin, FireEnd, ECC_GameTraceChannel1, CollisionQueryParams);

    AActor* HitActor = HitResult.GetActor();
    UPrimitiveComponent* HitComponent = HitResult.GetComponent();

    if (AnyHits && HitActor != nullptr) {
        AFightclashCharacter* Character = Cast<AFightclashCharacter>(HitActor);
        if (Character != nullptr) {
            UGameplayStatics::ApplyPointDamage(Character, Damage, FireDirection, HitResult, GetInstigatorController(), this, UDamageType::StaticClass());
            OnHitPlayer(Character, HitResult);
        }
        else {
            if (HitComponent != nullptr && HitComponent->IsSimulatingPhysics()) {
                FVector Impulse = FireDirection * Damage * 1000.0f;
                UE_LOG(LogTemp, Display, TEXT("%s applying impulse (%s) at (%s) to target component: "),
                    *GetNameSafe(Instigator), *Impulse.ToCompactString(),
                    *HitResult.Location.ToCompactString(), *GetNameSafe(HitComponent));
                HitComponent->AddImpulseAtLocation(Impulse, HitResult.Location);
            }
            OnHitObject(HitActor, HitResult);
        }
        OnHitEffects(HitResult);
    }

    AddRecoilAndSpreadForSingleShot();

#if !UE_BUILD_SHIPPING
    if (bShowDebugData) {
        UE_LOG(LogTemp, Display, TEXT("Shot hit at location: %s"), *HitResult.Location.ToCompactString());
        DrawDebugPoint(World, HitResult.Location, 5.f, FColor::Red, false, 5.f);
        /*
         * DrawDebugPoint(World, FireOrigin, 5.f, FColor::Yellow, false, 5.f);
         * DrawDebugLine(World, FireOrigin, HitResult.Location, FColor::Red, false, 5.f);
         * UE_LOG(LogTemp, Display, TEXT("Shot fired by %s with weapon %s hit target: %s, %s"),
         *     *GetNameSafe(Instigator), *GetNameSafe(this),
         *     *GetNameSafe(HitActor), *GetNameSafe(HitComponent));
         */
    }
#endif
}

void AWeapon_Hitscan::AddRecoilAndSpreadForSingleShot() {
    CurrentSpread = FMath::Min(CurrentSpread + SpreadGrowthPerShot, MaxSpread);
    FVector2D UnclampedRecoil = CurrentRecoil + FVector2D(RecoilPerShot.X * FMath::FRandRange(-1.f, 1.f), RecoilPerShot.Y * FMath::FRandRange(0.5f, 1.f));
    SetRecoil(FVector2D(FMath::Clamp(UnclampedRecoil.X, -MaxRecoil.X, MaxRecoil.X), FMath::Min(UnclampedRecoil.Y, MaxRecoil.Y)));
}

void AWeapon_Hitscan::SetRecoil(FVector2D NewRecoil) {
    FVector2D RecoilDelta = NewRecoil - CurrentRecoil;
    AController* Controller = GetInstigatorController();
    if (Controller != nullptr) {
        FRotator NewRotation = Controller->GetControlRotation();
        NewRotation.Add(RecoilDelta.Y, RecoilDelta.X, 0.f);
        GetInstigatorController()->SetControlRotation(NewRotation);
    }
    CurrentRecoil = NewRecoil;
}

void AWeapon_Hitscan::OnHitEffects_Implementation(const FHitResult& HitResult) {
    if (GetNetMode() == ENetMode::NM_DedicatedServer) {
        return;
    }

    if (Cast<APawn>(HitResult.Actor) != nullptr || BulletDecal == nullptr) {
        return;
    }

    if (BulletDecal == nullptr) {
        UE_LOG(LogTemp, Warning, TEXT("Hitscan Weapon %s hit an object but had no BulletDecal assigned"), *GetNameSafe(this));
        return;
    }

    UWorld* World = GetWorld();
    check(World);

    ADecalActor* Decal = World->SpawnActor<ADecalActor>(HitResult.Location, FRotator::ZeroRotator);
    if (Decal != nullptr) {
        Decal->SetDecalMaterial(BulletDecal);
        Decal->GetDecal()->SetRelativeRotation(HitResult.Normal.ToOrientationRotator());
        Decal->GetDecal()->DecalSize = BulletDecalSize;
        Decal->GetDecal()->SetFadeOut(BulletDecalDuration, BulletDecalFadeTime, true);
        Decal->GetDecal()->SetFadeScreenSize(0.001f);
    }
}
