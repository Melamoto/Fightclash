// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "BoostPickup.generated.h"

/**
 * 
 */
UCLASS()
class FIGHTCLASH_API ABoostPickup : public APickup
{
	GENERATED_BODY()

public:
    ABoostPickup();

    virtual void OnPickedUp(class AFightclashCharacter* Character) override;
	
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Pickup|Boost", meta = (ClampMin = 0.f, ClampMax = 1.f))
    float HealPercentage;
	
};
