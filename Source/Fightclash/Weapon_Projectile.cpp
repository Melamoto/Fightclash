// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon_Projectile.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "FightclashProjectile.h"

void AWeapon_Projectile::FireSingleShot() {
    // try and fire a projectile
    check(ProjectileClass);
    UWorld* const World = GetWorld();
    check(World);
    FVector FireDirection;
    FVector SpawnLocation;
    GetShotOriginAndDirection(SpawnLocation, FireDirection);
    FRotator SpawnRotation = FireDirection.Rotation();
    //Set Spawn Collision Handling Override
    FActorSpawnParameters ActorSpawnParams;
    ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
    ActorSpawnParams.Instigator = Instigator;

    // spawn the projectile at the muzzle
    AFightclashProjectile* Projectile = World->SpawnActor<AFightclashProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

#if !UE_BUILD_SHIPPING
    if (bShowDebugData) {
        FColor DebugColor = Projectile == nullptr ? FColor::Red : FColor::Yellow;
        DrawDebugSphere(World, SpawnLocation, 5.f, 4, DebugColor, false, 10.f);
    }
#endif
}
