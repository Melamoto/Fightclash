// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FightclashProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/DamageType.h"
#include "Kismet/GameplayStatics.h"
#include "FightclashCharacter.h"

AFightclashProjectile::AFightclashProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComp"));
	CollisionComp->InitSphereRadius(5.0f);
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &AFightclashProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

    SetReplicates(true);

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;

    Damage = 35.f;
}

void AFightclashProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) {

	// Only cause effect and destroy projectile if we hit another actor on the server
    if (Role != ROLE_Authority || OtherActor == nullptr || OtherActor == this) {
        return;
    }
    AFightclashCharacter* Character = Cast<AFightclashCharacter>(OtherActor);
    if (Character != nullptr) {
        if (Character != Instigator) {
            UGameplayStatics::ApplyPointDamage(Character, Damage, GetVelocity().GetSafeNormal(), Hit, GetInstigatorController(), this, UDamageType::StaticClass());
            OnHitPlayer(Character, Hit);
            Destroy();
        }
    }
    else if (OtherComp != nullptr) {
        if (OtherComp->IsSimulatingPhysics()) {
            OtherComp->AddImpulseAtLocation(GetVelocity() * 100.0f, GetActorLocation());
        }
        bool ShouldHitOtherComp = !ProjectileMovement->bShouldBounce || OtherComp->IsSimulatingPhysics();
        if (ShouldHitOtherComp) {
            OnHitObject(OtherActor, Hit);
            Destroy();
        }
    }
}
