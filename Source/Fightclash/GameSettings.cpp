// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSettings.h"
#include "Engine.h"
#include "Fightclash.h"
#include "GameFramework/InputSettings.h"
#include "GameFramework/GameUserSettings.h"

const uint8 MaximumBindingsPerInput = 2;

UGameSettings::UGameSettings() {
    static ConstructorHelpers::FObjectFinder<USoundMix> MasterSoundMixFinder(TEXT("/Game/Audio/MasterSoundMix"));
    MasterSoundMix = MasterSoundMixFinder.Object;

    static ConstructorHelpers::FObjectFinder<USoundClass> MusicSoundClassFinder(TEXT("/Game/Audio/MusicSoundClass"));
    MusicSoundClass = MusicSoundClassFinder.Object;
    static ConstructorHelpers::FObjectFinder<USoundClass> EffectsSoundClassFinder(TEXT("/Game/Audio/EffectsSoundClass"));
    EffectsSoundClass = EffectsSoundClassFinder.Object;
}

void UGameSettings::LoadFullConfig() {
    UE_LOG(LogTemp, Display, TEXT("UGameSettings: Loading full config..."));
    LoadConfig(UGameSettings::StaticClass());

    // Build KeyMap lists from defaults if needed
    if (!bUserHasCustomMappings) {
        ResetKeybindingsToDefaults();
    }

    UGameUserSettings* GameUserSettings = GetGameUserSettings();
    check(GameUserSettings);

    GameUserSettings->LoadSettings(true);
    ViewDistanceQuality = GameUserSettings->GetViewDistanceQuality();
    AntiAliasingQuality = GameUserSettings->GetAntiAliasingQuality();
    ShadowQuality = GameUserSettings->GetShadowQuality();
    PostProcessQuality = GameUserSettings->GetPostProcessingQuality();
    TextureQuality = GameUserSettings->GetTextureQuality();
    EffectsQuality = GameUserSettings->GetVisualEffectQuality();
    FoliageQuality = GameUserSettings->GetFoliageQuality();
            
    FIntPoint CurrentResolution = GameUserSettings->GetLastConfirmedScreenResolution();
    ResWidth = CurrentResolution.X;
    ResHeight = CurrentResolution.Y;
    FullscreenMode = GameUserSettings->GetFullscreenMode();
}

void UGameSettings::SaveFullConfig() {
    SaveConfig();
    UGameUserSettings* GameUserSettings = GetGameUserSettings();
    check(GameUserSettings);
    GameUserSettings->SetViewDistanceQuality(ViewDistanceQuality);
    GameUserSettings->SetAntiAliasingQuality(AntiAliasingQuality);
    GameUserSettings->SetShadowQuality(ShadowQuality);
    GameUserSettings->SetPostProcessingQuality(PostProcessQuality);
    GameUserSettings->SetTextureQuality(TextureQuality);
    GameUserSettings->SetVisualEffectQuality(EffectsQuality);
    GameUserSettings->SetFoliageQuality(FoliageQuality);

    GameUserSettings->SaveSettings();
}

void UGameSettings::ChangeScreenResolution(const int32 NewWidth, const int32 NewHeight, const EWindowMode::Type NewFullscreenMode) {
    UGameUserSettings* GameUserSettings = GetGameUserSettings();
    check(GameUserSettings);
    GameUserSettings->RequestResolutionChange(NewWidth, NewHeight, NewFullscreenMode, false);
}

void UGameSettings::ConfirmCurrentScreenResolution(bool IsAcceptable) {
    UGameUserSettings* GameUserSettings = GetGameUserSettings();
    check(GameUserSettings);
    if (IsAcceptable) {
        GameUserSettings->ConfirmVideoMode();
        // Local settings
        FIntPoint CurrentResolution = GameUserSettings->GetLastConfirmedScreenResolution();
        ResWidth = CurrentResolution.X;
        ResHeight = CurrentResolution.Y;
        FullscreenMode = GameUserSettings->GetFullscreenMode();
        // Game user settings
        GameUserSettings->SetScreenResolution(CurrentResolution);
        GameUserSettings->SetFullscreenMode(FullscreenMode.GetValue());
    }
    else {
        GameUserSettings->RevertVideoMode();
        GameUserSettings->ApplyResolutionSettings(false);
    }
}

void UGameSettings::ApplyCurrentConfig() {
    // Apply input settings
    ApplyInputOverrideMappings();

    // Apply audio settings
    UGameplayStatics::SetSoundMixClassOverride(this, MasterSoundMix, MusicSoundClass, MusicVolume * MasterVolume, 1.f, 0.f);
    UGameplayStatics::SetSoundMixClassOverride(this, MasterSoundMix, EffectsSoundClass, EffectsVolume * MasterVolume, 1.f, 0.f);

    // Apply graphics settings
    UGameUserSettings* GameUserSettings = GetGameUserSettings();
    check(GameUserSettings);
    GameUserSettings->SetViewDistanceQuality(ViewDistanceQuality);
    GameUserSettings->SetAntiAliasingQuality(AntiAliasingQuality);
    GameUserSettings->SetShadowQuality(ShadowQuality);
    GameUserSettings->SetPostProcessingQuality(PostProcessQuality);
    GameUserSettings->SetTextureQuality(TextureQuality);
    GameUserSettings->SetVisualEffectQuality(EffectsQuality);
    GameUserSettings->SetFoliageQuality(FoliageQuality);

    GameUserSettings->ApplyNonResolutionSettings();
}

UGameUserSettings* UGameSettings::GetGameUserSettings() {
    return GEngine != nullptr ? GEngine->GameUserSettings : nullptr;
}

void UGameSettings::ApplyInputOverrideMappings() {
    for (TObjectIterator<UPlayerInput> It; It; ++It)
    {
        ApplySettingsToPlayerInput(*It);
    }
}

bool UGameSettings::AddNewActionMapping(FInputActionKeyMapping NewMapping, uint8 Position, TArray<FInputBindingResult>& RemovedBindings) {
    bool ExistingBindingRemoved = false;
    ExistingBindingRemoved |= RemoveActionMappingForInputChord(NewMapping.Key, NewMapping.bShift, NewMapping.bCtrl, NewMapping.bAlt, NewMapping.bCmd, RemovedBindings);
    ExistingBindingRemoved |= RemoveAxisMappingForKey(NewMapping.Key, RemovedBindings);

    InsertActionKeyMapping(NewMapping, Position);

    return ExistingBindingRemoved;
}

bool UGameSettings::AddNewAxisMapping(FInputAxisKeyMapping NewMapping, uint8 Position, TArray<FInputBindingResult>& RemovedBindings) {
    bool ExistingBindingRemoved = false;
    ExistingBindingRemoved |= RemoveActionMappingForKey(NewMapping.Key, RemovedBindings);
    ExistingBindingRemoved |= RemoveAxisMappingForKey(NewMapping.Key, RemovedBindings);

    InsertAxisKeyMapping(NewMapping, Position);

    bUserHasCustomMappings = true;

    return ExistingBindingRemoved;
}

void UGameSettings::ResetKeybindingsToDefaults() {
    bUserHasCustomMappings = false;
    UInputSettings* InputSettings = UInputSettings::GetInputSettings();
    ActionMappings = InputSettings->ActionMappings;
    ActionMappings.StableSort(UGameSettings::CompareActionMappingOrder);
    AxisMappings = InputSettings->AxisMappings;
    AxisMappings.StableSort(UGameSettings::CompareAxisMappingOrder);
}

void UGameSettings::ApplySettingsToPlayerInput(UPlayerInput* PlayerInput) {
    PlayerInput->ActionMappings = ActionMappings;
    PlayerInput->AxisMappings = AxisMappings;
    PlayerInput->ForceRebuildingKeyMaps(false);
}

bool UGameSettings::RemoveActionMappingForInputChord(FKey Key, bool bShift, bool bCtrl, bool bAlt, bool bCmd, TArray<FInputBindingResult>& RemovedBindings) {
    for (FInputActionKeyMapping& KeyMapping : ActionMappings) {
        if (KeyMapping.Key == Key, KeyMapping.bShift == bShift, KeyMapping.bCtrl == bCtrl, KeyMapping.bAlt == bAlt, KeyMapping.bCmd == bCmd) {
            RemovedBindings.Add(FInputBindingResult(KeyMapping.ActionName));
            ActionMappings.RemoveSingle(KeyMapping);
            bUserHasCustomMappings = true;
            return true;
        }
    }
    return false;
}

bool UGameSettings::RemoveActionMappingForKey(FKey Key, TArray<FInputBindingResult>& RemovedBindings) {
    bool Result = false;
    for (FInputActionKeyMapping& KeyMapping : ActionMappings) {
        if (KeyMapping.Key == Key) {
            RemovedBindings.Add(FInputBindingResult(KeyMapping.ActionName));
            ActionMappings.RemoveSingle(KeyMapping);
            bUserHasCustomMappings = true;
            Result = true;
        }
    }
    return Result;
}

bool UGameSettings::RemoveAxisMappingForKey(FKey Key, TArray<FInputBindingResult>& RemovedBindings) {
    for (FInputAxisKeyMapping& KeyMapping : AxisMappings) {
        if (KeyMapping.Key == Key) {
            RemovedBindings.Add(FInputBindingResult(KeyMapping.AxisName, KeyMapping.Scale));
            AxisMappings.RemoveSingle(KeyMapping);
            bUserHasCustomMappings = true;
            return true;
        }
    }
    return false;
}

int32 UGameSettings::InsertActionKeyMapping(FInputActionKeyMapping NewMapping, uint8 Position) {
    bUserHasCustomMappings = true;
    uint8 PositionInMappingGroup = 0;
    // Array is sorted so binary search may be implemented, but performance improvements here are currently of negligible importance
    for (int32 MappingIndex = 0; MappingIndex < ActionMappings.Num(); ++MappingIndex) {
        int32 MappingGroupDirection = CompareActionMappingOrder(NewMapping, ActionMappings[MappingIndex]);
        if (MappingGroupDirection < 0) {
            continue;
        }
        if (MappingGroupDirection > 0 || PositionInMappingGroup == Position) {
            ActionMappings.Insert(NewMapping, MappingIndex);
            if (MappingGroupDirection == 0) {
                // Index at which the current mapping group should have ended due to exceeding the maximum number of keybinds per action
                int32 OversizedMappingGroupIndex = MappingIndex + (MaximumBindingsPerInput - PositionInMappingGroup);
                if (ActionMappings.IsValidIndex(OversizedMappingGroupIndex) && CompareActionMappingOrder(NewMapping, ActionMappings[OversizedMappingGroupIndex]) == 0) {
                    ActionMappings.RemoveAt(OversizedMappingGroupIndex);
                }
            }
            return MappingIndex;
        }
        PositionInMappingGroup += 1;
    }
    ActionMappings.Add(NewMapping);
    return ActionMappings.Num() - 1;
}

int32 UGameSettings::InsertAxisKeyMapping(FInputAxisKeyMapping NewMapping, uint8 Position) {
    bUserHasCustomMappings = true;
    uint8 PositionInMappingGroup = 0;
    // Array is sorted so binary search may be implemented, but performance improvements here are currently of negligible importance
    for (int32 MappingIndex = 0; MappingIndex < AxisMappings.Num(); ++MappingIndex) {
        int32 MappingGroupDirection = CompareAxisMappingOrder(NewMapping, AxisMappings[MappingIndex]);
        if (MappingGroupDirection < 0) {
            continue;
        }
        if (MappingGroupDirection > 0 || PositionInMappingGroup == Position) {
            AxisMappings.Insert(NewMapping, MappingIndex);
            if (MappingGroupDirection == 0) {
                // Index at which the current mapping group should have ended due to exceeding the maximum number of keybinds per axis
                int32 OversizedMappingGroupIndex = MappingIndex + (MaximumBindingsPerInput - PositionInMappingGroup);
                if (AxisMappings.IsValidIndex(OversizedMappingGroupIndex) && CompareAxisMappingOrder(NewMapping, AxisMappings[OversizedMappingGroupIndex]) == 0) {
                    AxisMappings.RemoveAt(OversizedMappingGroupIndex);
                }
            }
            return MappingIndex;
        }
        PositionInMappingGroup += 1;
    }
    AxisMappings.Add(NewMapping);
    return AxisMappings.Num() - 1;
}

int32 UGameSettings::CompareActionMappingOrder(FInputActionKeyMapping A, FInputActionKeyMapping B) {
    if (A.ActionName != B.ActionName) {
        return A.ActionName.Compare(B.ActionName);
    }
    return A.Key.IsGamepadKey() - B.Key.IsGamepadKey();
}

int32 UGameSettings::CompareAxisMappingOrder(FInputAxisKeyMapping A, FInputAxisKeyMapping B) {
    if (A.AxisName != B.AxisName) {
        return A.AxisName.Compare(B.AxisName);
    }
    if (A.Scale != B.Scale) {
        return A.Scale - B.Scale;
    }
    return A.Key.IsGamepadKey() - B.Key.IsGamepadKey();
}

bool UGameSettings::KeysHaveSameInputMode(FKey A, FKey B) {
    return A.IsGamepadKey() == B.IsGamepadKey();
}
