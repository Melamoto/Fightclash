// Fill out your copyright notice in the Description page of Project Settings.

#include "FCPlayerController.h"
#include "GameFramework/PlayerInput.h"
#include "FCGameInstance.h"
#include "Net/UnrealNetwork.h"
#include "FightclashCharacter.h"
#include "FCSpectator.h"
#include "GameSettings.h"
#include "FCPlayerState.h"

void AFCPlayerController::SetupInputComponent() {
    Super::SetupInputComponent();
    InputComponent->BindAction("Toggle Ready", IE_Pressed, this, &AFCPlayerController::ToggleReady);
}

void AFCPlayerController::InitInputSystem() {
    Super::InitInputSystem();
    UFCGameInstance* FCGameInstance = Cast<UFCGameInstance>(GetGameInstance());
    if (FCGameInstance != nullptr) {
        UGameSettings* GameSettings = FCGameInstance->GetCurrentGameSettings();
        GameSettings->ApplySettingsToPlayerInput(PlayerInput);
    }
}

void AFCPlayerController::BeginSpectatingState() {
    Super::BeginSpectatingState();
    OnStartedSpectating();
}

void AFCPlayerController::BeginPlayingState() {
    Super::BeginPlayingState();
    FCCharacter = Cast<AFightclashCharacter>(GetPawn());
    if (FCCharacter != nullptr) {
        OnStartedPlaying();
    }
}

void AFCPlayerController::ToggleReady() {
    if (!IsInState(NAME_Spectating)) {
        return;
    }
    bPlayerIsWaiting = !bPlayerIsWaiting;
}
