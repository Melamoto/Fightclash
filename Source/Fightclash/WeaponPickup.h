// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Pickup.h"
#include "WeaponPickup.generated.h"

/**
 * 
 */
UCLASS()
class FIGHTCLASH_API AWeaponPickup : public APickup
{
	GENERATED_BODY()
	
public:
    AWeaponPickup();

    virtual void PostInitializeComponents() override;

    #if WITH_EDITOR
    virtual void PostEditChangeProperty(struct FPropertyChangedEvent & PropertyChangedEvent) override;
    #endif

    virtual void OnPickedUp(class AFightclashCharacter* Character) override;

    void UpdateWeaponMesh();

    UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly)
    class USkeletalMeshComponent* WeaponMesh;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Pickup|Weapon")
    TSubclassOf<class AWeapon> WeaponClass;

	
};
