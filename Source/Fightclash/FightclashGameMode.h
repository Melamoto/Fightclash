// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "FightclashGameMode.generated.h"

UCLASS(minimalapi)
class AFightclashGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AFightclashGameMode();

    virtual void BeginPlay() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Match")
    int32 MinimumPlayersToStartMatch;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Match")
    int32 ScoreNeededToWin;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Match")
    float PostGameTransitionTime;
    
    virtual bool ShouldSpawnAtStartSpot(AController* Player) override;

    virtual void PostLogin(APlayerController* NewPlayer) override;

    virtual void HandleMatchHasEnded() override;

    virtual bool ReadyToEndMatch_Implementation() override;

    virtual void RestartPlayer(AController* NewPlayer) override;

    UFUNCTION()
    void OnPlayerKilled(class AFightclashCharacter* KilledCharacter, AController* Killer);

    UFUNCTION(BlueprintNativeEvent)
    FString GetNextMapName();

    UFUNCTION(BlueprintCallable)
    void TransitionToNextMap();

    FTimerHandle MapTransitionTimer;
};
