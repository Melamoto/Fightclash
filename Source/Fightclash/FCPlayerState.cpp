// Fill out your copyright notice in the Description page of Project Settings.

#include "FCPlayerState.h"

AFCPlayerState::AFCPlayerState() {
    bReadyToStart = false;
}

bool AFCPlayerState::ReadyToPlayGame() {
    return !bIsSpectator || bReadyToStart;
}
