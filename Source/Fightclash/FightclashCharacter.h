// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FightclashCharacter.generated.h"

class UInputComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInstigatorDelegate, AFightclashCharacter*, Target, AController*, Instigator);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FInventoryDelegate, int32, InventoryIndex, class AWeapon*, Weapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUpdateDelegate);

UCLASS(config=Game)
class AFightclashCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	class USkeletalMeshComponent* Mesh1P;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	/** Motion controller (right hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* R_MotionController;

	/** Motion controller (left hand) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UMotionControllerComponent* L_MotionController;

public:
	AFightclashCharacter(const FObjectInitializer& ObjectInitializer);

    virtual void Destroyed();

    virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void BeginPlay() override;
    virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

    virtual void Tick(float DeltaSeconds) override;

    virtual void Jump();
    virtual void StopJumping();

// Inventory
public:
    UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, ReplicatedUsing = OnRep_CurrentWeapon, Category = "Gameplay | Inventory")
    class AWeapon* CurrentWeapon;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, ReplicatedUsing = OnRep_Inventory, Category = "Gameplay | Inventory")
    TArray<class AWeapon*> Inventory;

    UFUNCTION()
    void OnRep_CurrentWeapon(class AWeapon* LastWeapon);
    UFUNCTION()
    void OnRep_Inventory(TArray<class AWeapon*> PreviousInventory);

    UFUNCTION(BlueprintImplementableEvent, Category = "Gameplay | Inventory")
    void OnInventoryChanged();

    UPROPERTY(BlueprintAssignable, Category = "Gameplay | Inventory")
    FInventoryDelegate OnItemEquipped;
    UPROPERTY(BlueprintAssignable, Category = "Gameplay | Inventory")
    FInventoryDelegate OnItemAdded;
    UPROPERTY(BlueprintAssignable, Category = "Gameplay | Inventory")
    FInventoryDelegate OnItemRemoved;

    UFUNCTION(BlueprintCallable)
    void EquipNextWeapon();
    UFUNCTION(BlueprintCallable)
    void EquipPreviousWeapon();

    UFUNCTION(BlueprintCallable)
    void EquipWeapon(class AWeapon* Weapon);

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerEquipWeapon(class AWeapon* Weapon);
    void ServerEquipWeapon_Implementation(class AWeapon* Weapon);
    bool ServerEquipWeapon_Validate(class AWeapon* Weapon);

    UFUNCTION(BlueprintCallable)
    void AddWeaponToInventory(class AWeapon* Weapon);

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerAddWeaponToInventory(class AWeapon* Weapon);
    void ServerAddWeaponToInventory_Implementation(class AWeapon* Weapon);
    bool ServerAddWeaponToInventory_Validate(class AWeapon* Weapon);

    void SetCurrentWeapon(class AWeapon* NewWeapon, class AWeapon* LastWeapon);

public:
    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Gameplay|Mesh")
    FName GripSocketName1P;

    UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Gameplay|Mesh")
    FName GripSocketName3P;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Mesh")
    TMap<FName, float> BodyDamageModifiers;

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

	/** Whether to use motion controller location for aiming. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	uint32 bUsingMotionControllers : 1;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Match)
    int PlayerTeam;

    FInstigatorDelegate OnKilledDelegate;

public:
    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Gameplay)
    float MaxHealth;

    UPROPERTY(VisibleInstanceOnly, BlueprintReadWrite, ReplicatedUsing = OnRep_Health, Category = Gameplay)
    float Health;

    UFUNCTION()
    void OnRep_Health();

    UPROPERTY(BlueprintAssignable, Category = "Gameplay | Status")
    FUpdateDelegate OnHealthUpdated;

public:

    bool bWantsToFire;

    // [local] starts firing weapon
    void StartFiring();
    // [local] stops firing weapon
    void StopFiring();

    UFUNCTION(Server, Reliable, WithValidation)
    void ServerTakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
    void ServerTakeDamage_Implementation(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
    bool ServerTakeDamage_Validate(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser);

    UFUNCTION(Server, Unreliable, WithValidation)
    void ServerSetJump(bool Jumping);
    void ServerSetJump_Implementation(bool Jumping);
    bool ServerSetJump_Validate(bool Jumping);

	/** Resets HMD orientation and position in VR. */
	void OnResetVR();

//////////////////////////////////////////////////////////////////////////
// Movement
protected:
    virtual bool CanJumpInternal_Implementation() const override;

public:
    UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Replicated, Category = "Gameplay|Movement")
    uint32 bIsJumping : 1;

    UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Replicated, Category = "Gameplay|Movement")
    FRotator AimOffset;

//////////////////////////////////////////////////////////////////////////
// Input
	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

    void PerformDodge();

    void StartSprinting();

    void StopSprinting();

	struct TouchData
	{
		TouchData() { bIsPressed = false;Location=FVector::ZeroVector;}
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);
	TouchData	TouchItem;
	
protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;
	// End of APawn interface

	/* 
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so 
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	bool EnableTouchscreenMovement(UInputComponent* InputComponent);

public:
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
    /** Returns Mesh3P subobject **/
    FORCEINLINE class USkeletalMeshComponent* GetMesh3P() const { return GetMesh(); }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }

};

