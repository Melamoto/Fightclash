// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "Weapon_Hitscan.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class FIGHTCLASH_API AWeapon_Hitscan : public AWeapon
{
    GENERATED_BODY()
public:
    AWeapon_Hitscan();

    virtual void Tick(float DeltaSeconds) override;
    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    virtual void FireSingleShot() override;
    virtual void FireSingleShotLocal() override;
    virtual void GetShotOriginAndDirection(FVector& Origin, FVector& Direction) const;

    void SetRecoil(FVector2D Recoil);

    void AddRecoilAndSpreadForSingleShot();

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Weapon Cosmetics")
    UMaterialInterface* BulletDecal;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Weapon Cosmetics")
    FVector BulletDecalSize;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Weapon Cosmetics")
    float BulletDecalDuration;
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay|Weapon Cosmetics")
    float BulletDecalFadeTime;
    

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    float MaxRange;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    float Damage;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    float MaxSpread;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    float SpreadGrowthPerShot;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    float SpreadDecayPerSecond;
    UPROPERTY(BlueprintReadWrite, Transient, Replicated, EditInstanceOnly, Category = "Gameplay|Weapon Properties")
    float CurrentSpread;

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    FVector2D MaxRecoil;
    UPROPERTY(BlueprintReadWrite, Transient, Replicated, EditInstanceOnly, Category = "Gameplay|Weapon Properties")
    FVector2D RecoilPerShot;
    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Gameplay|Weapon Properties")
    float RecoilRecoveryPerSecond;
    UPROPERTY(BlueprintReadWrite, Transient, Replicated, EditInstanceOnly, Category = "Gameplay|Weapon Properties")
    FVector2D CurrentRecoil;

    UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly, Category = "Weapon|Hitscan")
    void OnHitPlayer(class AFightclashCharacter* Player, const FHitResult& HitResult);

    UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly, Category = "Weapon|Hitscan")
    void OnHitObject(AActor* Object, const FHitResult& HitResult);

    // [server] create any cosmetic effects resulting from hitting anything, replicates to clients
    UFUNCTION(NetMulticast, Unreliable, Category = "Hitscan")
    void OnHitEffects(const FHitResult& HitResult);
};
