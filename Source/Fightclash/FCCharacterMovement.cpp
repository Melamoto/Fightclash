// Fill out your copyright notice in the Description page of Project Settings.

#include "FCCharacterMovement.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "Engine.h"

UFCCharacterMovement::UFCCharacterMovement() {
    SprintSpeedMultiplier = 2.f;
    SprintAccelerationMultiplier = 2.f;

    DodgeStrength = 2000.f;
    GroundDodgeStrengthMultiplier = 1.5f;

    DodgeJumpSpeedThreshold = 300.f;

    MaxJumpCount = 2;
    JumpCount = 0;

    WallslideStrength = 4.f;
}

void UFCCharacterMovement::SetSprinting(bool bShouldSprint) {
    bWantsToSprint = bShouldSprint;
}

void UFCCharacterMovement::PerformDodgeInMoveDirection() {
    if (CharacterOwner != nullptr) {
        PerformDodge(CharacterOwner->GetLastMovementInputVector());
    }
}

void UFCCharacterMovement::PerformDodge(FVector DodgeDir) {
    bWantsToDodge = true;
    DodgeDirection = DodgeDir;
    if (CharacterOwner->Role < ROLE_Authority) {
        ServerPerformDodge(DodgeDirection);
    }
}

float UFCCharacterMovement::GetMaxSpeed() const {
    float MaxSpeed = Super::GetMaxSpeed();
    
    if (bWantsToSprint && IsMovingForward()) {
        MaxSpeed *= SprintSpeedMultiplier;
    }

    return MaxSpeed;
}

float UFCCharacterMovement::GetMaxAcceleration() const {
    float MaxAcceleration = Super::GetMaxAcceleration();

    if (bWantsToSprint && IsMovingForward()) {
        MaxAcceleration *= SprintAccelerationMultiplier;
    }

    return MaxAcceleration;
}

void UFCCharacterMovement::OnMovementUpdated(float DeltaSeconds, const FVector& OldLocation, const FVector& OldVelocity) {
    Super::OnMovementUpdated(DeltaSeconds, OldLocation, OldVelocity);

    if (CharacterOwner == nullptr) {
        return;
    }

    if (bWantsToDodge) {
        if (DodgeDirection.IsNearlyZero()) {
            DodgeDirection = GetOwner()->GetActorForwardVector();
        }
        DodgeDirection.Normalize();
        DodgeDirection.Z = 0.f;
        FVector DodgeVelocity = DodgeDirection * DodgeStrength;

        if (IsMovingOnGround()) {
            DodgeVelocity *= GroundDodgeStrengthMultiplier;
        }
        else {
            DodgeVelocity.Z = Velocity.Z;
        }

        Launch(DodgeVelocity);
        bWantsToDodge = false;
    }
}

FVector UFCCharacterMovement::NewFallVelocity(const FVector& InitialVelocity, const FVector& Gravity, float DeltaTime) const {
    FVector Result = Super::NewFallVelocity(InitialVelocity, Gravity, DeltaTime);
    // Wallslide
    FVector MoveDirection = GetLastInputVector();
    if (Result.Z >= 0 || MoveDirection.IsNearlyZero()) {
        return Result;
    }
    // Check for candidate wall
    float CapsuleRadius = GetCharacterOwner()->GetCapsuleComponent()->GetScaledCapsuleRadius();
    float TraceDistance = CapsuleRadius * 1.4; // Should detect a player moving at a little under a 45 degree angle to the wall
    FVector TraceStart = GetActorLocation();
    FVector TraceEnd = TraceStart + (MoveDirection * TraceDistance);
    UWorld* World = GetWorld();
    check(World);
    FHitResult HitResult;
    FCollisionQueryParams QueryParams(FName("Wallslide Test"), false, GetOwner());
    World->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_WorldStatic, QueryParams);
    if (HitResult.bBlockingHit && Cast<AStaticMeshActor>(HitResult.Actor) != nullptr) {
        // Check if wall is close enough
        FVector ImpactToPlayer = GetOwner()->GetActorLocation() - HitResult.Location;
        float WallDistance = FVector::DotProduct(ImpactToPlayer, HitResult.Normal);
        if (WallDistance < CapsuleRadius + 1.f) {
            Result.Z -= WallslideStrength * Result.Z * DeltaTime;
        }
    }
    return Result;
}

void UFCCharacterMovement::ServerPerformDodge_Implementation(const FVector& DodgeDir) {
    DodgeDirection = DodgeDir;
}

bool UFCCharacterMovement::ServerPerformDodge_Validate(const FVector& DodgeDir) {
    return true;
}

bool UFCCharacterMovement::DoJump(bool bReplayingMoves) {
    bool StartingFromGround = IsMovingOnGround();
    if (!Super::DoJump(bReplayingMoves)) {
        return false;
    }
    JumpCount += 1;
    FVector2D HorizontalVelocity = FVector2D(Velocity.X, Velocity.Y);
    UE_LOG(LogTemp, Warning, TEXT("Starting from ground? %s : Hor %s"), StartingFromGround ? TEXT("true") : TEXT("false"), *HorizontalVelocity.ToString());
    if (StartingFromGround && HorizontalVelocity.SizeSquared() > (DodgeJumpSpeedThreshold * DodgeJumpSpeedThreshold)) {
        UE_LOG(LogTemp, Warning, TEXT("Reduced horizontal velocity: Previous size %f, Next %f"), HorizontalVelocity.Size(), DodgeJumpSpeedThreshold);
        float SpeedReduction = HorizontalVelocity.Size() - DodgeJumpSpeedThreshold;
        float SpeedProportion = DodgeJumpSpeedThreshold / HorizontalVelocity.Size();
        Velocity.X *= SpeedProportion;
        Velocity.Y *= SpeedProportion;
        Velocity.Z += SpeedReduction / 2;
    }
    return true;
}

void UFCCharacterMovement::ProcessLanded(const FHitResult& Hit, float RemainingTime, int32 Iterations) {
    Super::ProcessLanded(Hit, RemainingTime, Iterations);
    JumpCount = 0;
}

bool UFCCharacterMovement::CanJump() const {
    return (IsMovingOnGround() || JumpCount < MaxJumpCount) && CanEverJump();
}

bool UFCCharacterMovement::IsMovingForward() const {
    if (PawnOwner == nullptr) {
        return false;
    }
    FVector Forward = PawnOwner->GetActorForwardVector();
    FVector MoveDirection = Velocity.GetSafeNormal();

    Forward.Z = 0.f;
    MoveDirection.Z = 0.f;

    float VelocityDot = FVector::DotProduct(Forward, MoveDirection);
    return VelocityDot > 0.7;
}

FNetworkPredictionData_Client* UFCCharacterMovement::GetPredictionData_Client() const {
    if (ClientPredictionData == nullptr) {
        UFCCharacterMovement* MutableThis = const_cast<UFCCharacterMovement*>(this);
        MutableThis->ClientPredictionData = new FNetworkPredictionData_Client_FCCharacter(*this);
    }

    return ClientPredictionData;
}

void UFCCharacterMovement::UpdateFromCompressedFlags(uint8 Flags) {
    Super::UpdateFromCompressedFlags(Flags);

    bWantsToSprint = (Flags & SPRINT_FLAG) != 0;
    bWantsToDodge = (Flags & DODGE_FLAG) != 0;
}

//////////////////////////////////////////////////////////////////////////
// Replicated State

void FSavedMove_FCCharacter::Clear() {
    FSavedMove_Character::Clear();
    bWantsToSprint = false;
    bWantsToDodge = false;
    DodgeDirection = FVector::ZeroVector;
    JumpCount = 0;
}

uint8 FSavedMove_FCCharacter::GetCompressedFlags() const {
    uint8 Result = FSavedMove_Character::GetCompressedFlags();

    if (bWantsToSprint) {
        Result |= UFCCharacterMovement::SPRINT_FLAG;
    }
    if (bWantsToDodge) {
        Result |= UFCCharacterMovement::DODGE_FLAG;
    }
    
    return Result;
}

bool FSavedMove_FCCharacter::CanCombineWith(const FSavedMovePtr& NewMove, ACharacter* Character, float MaxDelta) const {

    if (bWantsToSprint != ((FSavedMove_FCCharacter*)&NewMove)->bWantsToSprint) {
        return false;
    }
    if (bWantsToDodge != ((FSavedMove_FCCharacter*)&NewMove)->bWantsToDodge) {
        return false;
    }
    if (DodgeDirection != ((FSavedMove_FCCharacter*)&NewMove)->DodgeDirection) {
        return false;
    }
    if (JumpCount != ((FSavedMove_FCCharacter*)&NewMove)->JumpCount) {
        return false;
    }

    return FSavedMove_Character::CanCombineWith(NewMove, Character, MaxDelta);
}

void FSavedMove_FCCharacter::SetMoveFor(ACharacter* Character, float InDeltaTime, FVector const& NewAccel, FNetworkPredictionData_Client_Character& ClientData) {
    FSavedMove_Character::SetMoveFor(Character, InDeltaTime, NewAccel, ClientData);

    UFCCharacterMovement* CharacterMovement = Cast<UFCCharacterMovement>(Character->GetCharacterMovement());
    if (CharacterMovement != nullptr) {
        bWantsToSprint = CharacterMovement->bWantsToSprint;
        bWantsToDodge = CharacterMovement->bWantsToDodge;
        DodgeDirection = CharacterMovement->DodgeDirection;
        JumpCount = CharacterMovement->JumpCount;
    }
}

void FSavedMove_FCCharacter::PrepMoveFor(ACharacter* Character) {
    FSavedMove_Character::PrepMoveFor(Character);

    UFCCharacterMovement* CharacterMovement = Cast<UFCCharacterMovement>(Character->GetCharacterMovement());
    if (CharacterMovement != nullptr) {
        CharacterMovement->bWantsToSprint = bWantsToSprint;
        CharacterMovement->bWantsToDodge = bWantsToDodge;
        CharacterMovement->DodgeDirection = DodgeDirection;
        CharacterMovement->JumpCount = JumpCount;
    }
}

FNetworkPredictionData_Client_FCCharacter::FNetworkPredictionData_Client_FCCharacter(const UCharacterMovementComponent& ClientMovement) : FNetworkPredictionData_Client_Character(ClientMovement) {
}

FSavedMovePtr FNetworkPredictionData_Client_FCCharacter::AllocateNewMove() {
    return FSavedMovePtr(new FSavedMove_FCCharacter());
}
