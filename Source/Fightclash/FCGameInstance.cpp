// Fill out your copyright notice in the Description page of Project Settings.

#include "FCGameInstance.h"
#include "GameSettings.h"
#include "GameFramework/GameUserSettings.h"
#include "Engine.h"

UGameSettings* UFCGameInstance::GetCurrentGameSettings() {
    if (GameSettings == nullptr || !GameSettings->IsValidLowLevel()) {
        UE_LOG(LogTemp, Display, TEXT("UFCGameInstance (%lu): No valid GameSettings (exists = %s), loading..."), this, GameSettings != nullptr ? TEXT("true") : TEXT("false"));
        GameSettings = NewObject<UGameSettings>(this);
        GameSettings->LoadFullConfig();
    }
    return GameSettings;
}
