// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "FCGameState.generated.h"

UCLASS()
class FIGHTCLASH_API AFCGameState : public AGameState
{
    GENERATED_BODY()

public:
    AFCGameState();

    virtual void HandleMatchHasEnded() override;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Match)
    uint32 bFriendlyFireEnabled : 1;
	
    UFUNCTION(BlueprintImplementableEvent, Category = Match)
    void OnMatchEnded();
	
};
