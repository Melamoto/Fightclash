// Fill out your copyright notice in the Description page of Project Settings.

#include "MenuController.h"
#include "Engine/World.h"


void AMenuController::JoinGame(FString ServerURL) {
    ClientTravel(ServerURL, ETravelType::TRAVEL_Absolute);
}

void AMenuController::HostGame(FString Map, FString Port) {
    UWorld* World = GetWorld();
    if (World != nullptr) {
        World->ServerTravel(Map + TEXT("?listen") + TEXT("?port=") + Port, ETravelType::TRAVEL_Absolute);
    }
}
