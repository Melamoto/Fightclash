// Fill out your copyright notice in the Description page of Project Settings.

#include "Pickup.h"
#include "Components/CapsuleComponent.h"
#include "TimerManager.h"
#include "Net/UnrealNetwork.h"
#include "FightclashCharacter.h"


// Sets default values
APickup::APickup()
{
    CollisionComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CollisionComponent"));
    RootComponent = CollisionComponent;
    CollisionComponent->OnComponentBeginOverlap.AddDynamic(this, &APickup::OnOverlapBegin);

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    bReplicates = true;

    RespawnTime = 3.f;

    bActive = true;
}

void APickup::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(APickup, bActive);
}

void APickup::OnRep_Active()
{
    OnActiveStatusChanged();
}

// Called when the game starts or when spawned
void APickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void APickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult)
{
    if (!bActive || Role != ROLE_Authority) {
        return;
    }
    AFightclashCharacter* Character = Cast<AFightclashCharacter>(OtherActor);
    if (Character == nullptr) {
        return;
    }
    UE_LOG(LogTemp, Display, TEXT("%s picked up by Character %s"), *GetName(), *Character->GetName());
    bActive = false;
    OnPickedUp(Character);
    GetWorldTimerManager().SetTimer(RespawnTimerHandle, this, &APickup::Respawn, RespawnTime, false);
    if (GetNetMode() != ENetMode::NM_DedicatedServer) {
        OnActiveStatusChanged();
    }
}

void APickup::Respawn() {
    bActive = true;
    if (GetNetMode() != ENetMode::NM_DedicatedServer) {
        OnRep_Active();
    }
}
