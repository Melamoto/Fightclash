// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Pickup.generated.h"

UCLASS(abstract, Blueprintable)
class FIGHTCLASH_API APickup : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APickup();

    virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Pickup")
    class UCapsuleComponent* CollisionComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

    UFUNCTION()
    virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepHitResult);
	
    UFUNCTION(BlueprintAuthorityOnly)
    virtual void OnPickedUp(class AFightclashCharacter* Character) PURE_VIRTUAL(APickup::OnPickedUp,);

    UFUNCTION(BlueprintImplementableEvent, Category = "Pickup")
    void OnActiveStatusChanged();

    UFUNCTION()
    void Respawn();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Pickup")
    float RespawnTime;

    FTimerHandle RespawnTimerHandle;

    UPROPERTY(BlueprintReadOnly, VisibleAnywhere, ReplicatedUsing = OnRep_Active, Category = "Pickup")
    uint8 bActive : 1;

    UFUNCTION()
    void OnRep_Active();
};
