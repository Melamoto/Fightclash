// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FightclashProjectile.generated.h"

UCLASS(config=Game)
class AFightclashProjectile : public AActor
{
	GENERATED_BODY()

	/** Sphere collision component */
	UPROPERTY(VisibleDefaultsOnly, Category=Projectile)
	class USphereComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

public:
	AFightclashProjectile();

    UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Projectile")
    float Damage;

	/** called when projectile hits something */
    UFUNCTION()
    void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

    UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly, Category = "Projectile")
    void OnHitPlayer(class AFightclashCharacter* Player, const FHitResult& HitResult);

    UFUNCTION(BlueprintImplementableEvent, BlueprintAuthorityOnly, Category = "Projectile")
    void OnHitObject(AActor* Object, const FHitResult& HitResult);

	/** Returns CollisionComp subobject **/
	FORCEINLINE class USphereComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }
};

