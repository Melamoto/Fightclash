// Fill out your copyright notice in the Description page of Project Settings.

#include "GameSettingsLib.h"
#include "GameSettings.h"
#include "GameFramework/GameUserSettings.h"
#include "Engine.h"


bool UGameSettingsLib::GetSupportedScreenResolutions(TArray<FString>& Resolutions) {
    FScreenResolutionArray ResolutionsArray;

    if (RHIGetAvailableResolutions(ResolutionsArray, true)) {
        for (const FScreenResolutionRHI& Resolution : ResolutionsArray) {
            float AspectRatio = (float)Resolution.Width / Resolution.Height;
            if (AspectRatio < 1.7f) {
                continue;
            }
            FString WidthString = FString::FromInt(Resolution.Width);
            FString HeightString = FString::FromInt(Resolution.Height);
            Resolutions.AddUnique(WidthString + "x" + HeightString);
        }
        return true;
    }
    return false;
}

void UGameSettingsLib::ResolutionStringToValues(FString Resolution, int32& Width, int32& Height) {
    TArray<FString> Resolutions;
    int32 Length = Resolution.ParseIntoArray(Resolutions, TEXT("x"), true);
    if (Length != 2) {
        Width = 0;
        Height = 0;
        return;
    }
    Width = FCString::Atoi(*Resolutions[0]);
    Height = FCString::Atoi(*Resolutions[1]);
}

EWindowMode::Type UGameSettingsLib::WindowModeStringToValue(FString WindowModeString) {
    if (WindowModeString.Equals("Fullscreen")) {
        return EWindowMode::Fullscreen;
    }
    if (WindowModeString.Equals("Windowed Fullscreen")) {
        return EWindowMode::WindowedFullscreen;
    }
    return EWindowMode::Windowed;
}

FString UGameSettingsLib::WindowModeToString(EWindowMode::Type WindowMode) {
    if (WindowMode == EWindowMode::Fullscreen) {
        return TEXT("Fullscreen");
    }
    if (WindowMode == EWindowMode::WindowedFullscreen) {
        return TEXT("Windowed Fullscreen");
    }
    return TEXT("Windowed");
}
