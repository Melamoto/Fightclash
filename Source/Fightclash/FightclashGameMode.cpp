// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "FightclashGameMode.h"
#include "FightclashHUD.h"
#include "FightclashCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "GameFramework/PlayerState.h"
#include "GameFramework/GameState.h"
#include "FightclashCharacter.h"
#include "FCGameInstance.h"
#include "GameSettings.h"
#include "FCGameState.h"
#include "FCPlayerState.h"
#include "Kismet/GameplayStatics.h"
#include "Engine.h"

AFightclashGameMode::AFightclashGameMode()
	: Super()
{
    MinimumPlayersToStartMatch = 2;
    ScoreNeededToWin = 3;
    GameStateClass = AFCGameState::StaticClass();
    PlayerStateClass = AFCPlayerState::StaticClass();
    MinRespawnDelay = 3.f;
    bUseSeamlessTravel = true;
    PostGameTransitionTime = 5.f;
}

void AFightclashGameMode::BeginPlay() {
    Super::BeginPlay();
    UFCGameInstance* GameInstance = Cast<UFCGameInstance>(GetGameInstance());
    if (GameInstance != nullptr) {
        UGameSettings* GameSettings = GameInstance->GetCurrentGameSettings();
        GameSettings->ApplyCurrentConfig();
    }
}

bool AFightclashGameMode::ShouldSpawnAtStartSpot(AController* Player) {
    return false;
}

void AFightclashGameMode::PostLogin(APlayerController* NewPlayer) {
    Super::PostLogin(NewPlayer);
    APlayerState* PlayerState = NewPlayer->PlayerState;
    if (PlayerState != nullptr) {
        if (PlayerState->GetPlayerName().IsEmpty()) {
            PlayerState->SetPlayerName(TEXT("Player ") + FString::FromInt(GetNumPlayers()));
        }
        PlayerState->Score = 0.f;
    }
}

void AFightclashGameMode::HandleMatchHasEnded() {
    Super::HandleMatchHasEnded();
    UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 0.6);
    GetWorldTimerManager().SetTimer(MapTransitionTimer, this, &AFightclashGameMode::TransitionToNextMap, PostGameTransitionTime * 0.6, false);
}

bool AFightclashGameMode::ReadyToEndMatch_Implementation()
{
    for (auto Player : GameState->PlayerArray) {
        if (Player->Score >= ScoreNeededToWin) {
            return true;
        }
    }
    return false;
}

void AFightclashGameMode::RestartPlayer(AController* NewPlayer) {
    Super::RestartPlayer(NewPlayer);
    AFightclashCharacter* PlayerCharacter = Cast<AFightclashCharacter>(NewPlayer->GetCharacter());
    if (PlayerCharacter == nullptr) {
        UE_LOG(LogTemp, Error, TEXT("Player Controller %s spawned with invalid character: %s"), *GetNameSafe(NewPlayer), *GetNameSafe(NewPlayer->GetPawn()));
        return;
    }
    PlayerCharacter->OnKilledDelegate.AddDynamic(this, &AFightclashGameMode::OnPlayerKilled);
}

FString AFightclashGameMode::GetNextMapName_Implementation() {
    UWorld* World = GetWorld();
    check(World);
    return World->GetLocalURL();
}

void AFightclashGameMode::TransitionToNextMap() {
    FString NextMapName = GetNextMapName();
    UWorld* World = GetWorld();
    check(World);
    World->ServerTravel(NextMapName);
}

void AFightclashGameMode::OnPlayerKilled(AFightclashCharacter* KilledCharacter, AController* Killer) {
    // Update Killer's score
    if (IsMatchInProgress() && Killer != nullptr && Killer->PlayerState != nullptr) {
        APlayerState* PlayerState = Killer->PlayerState;
        PlayerState->Score += 1;
    }
    // Set respawn timer
    float WorldRespawnTime = GameState->GetServerWorldTimeSeconds() + MinRespawnDelay;
    FTimerDelegate PlayerRespawnDelegate;
    FTimerHandle PlayerRespawnTimer;
    PlayerRespawnDelegate.BindUFunction(this, FName("RestartPlayer"), KilledCharacter->Controller);
    GetWorldTimerManager().SetTimer(PlayerRespawnTimer, PlayerRespawnDelegate, MinRespawnDelay, false);
}
