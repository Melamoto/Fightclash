// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "FCPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class FIGHTCLASH_API AFCPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
    AFCPlayerState();

    bool ReadyToPlayGame();

    UPROPERTY(BlueprintReadWrite, EditInstanceOnly, Category = "Gameplay | Match")
    uint32 bReadyToStart : 1;
	
};
