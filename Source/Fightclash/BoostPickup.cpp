// Fill out your copyright notice in the Description page of Project Settings.

#include "BoostPickup.h"
#include "FightclashCharacter.h"

ABoostPickup::ABoostPickup() {
    HealPercentage = 0.5f;
}

void ABoostPickup::OnPickedUp(AFightclashCharacter* Character) {
    Character->Health = FMath::Min(Character->MaxHealth, Character->Health + (Character->MaxHealth * HealPercentage));
    Character->OnHealthUpdated.Broadcast();
}
