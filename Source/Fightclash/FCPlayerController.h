// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FCPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class FIGHTCLASH_API AFCPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

    virtual void SetupInputComponent() override;
    virtual void InitInputSystem() override;

    virtual void BeginSpectatingState();
    virtual void BeginPlayingState();

    UFUNCTION(BlueprintImplementableEvent)
    void OnNewCharacter(class AFightclashCharacter* FCCharacter);

    UFUNCTION(BlueprintImplementableEvent)
    void OnCharacterRemoved();

    UFUNCTION(BlueprintImplementableEvent)
    void OnStartedSpectating();

    UFUNCTION(BlueprintImplementableEvent)
    void OnStartedPlaying();

    void ToggleReady();
	
    UPROPERTY(BlueprintReadOnly, VisibleInstanceOnly, Category = "Game|Pawn")
    class AFightclashCharacter* FCCharacter;
};
