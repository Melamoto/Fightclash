// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"
#include "FightclashCharacter.h"
#include "FightclashProjectile.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "FCPlayerController.h"
#include "DrawDebugHelpers.h"
#include "Engine.h"


AWeapon::AWeapon() {
    Mesh3P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh_TP"));
    Mesh3P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
    Mesh3P->SetOwnerNoSee(true);
    Mesh3P->bSelfShadowOnly = true;
    Mesh3P->SetCollisionObjectType(ECC_WorldDynamic);
    Mesh3P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    Mesh3P->SetCollisionResponseToAllChannels(ECR_Ignore);
    Mesh3P->SetIsReplicated(true);
    RootComponent = Mesh3P;

    Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh_FP"));
    Mesh1P->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
    Mesh1P->SetOnlyOwnerSee(true);
    Mesh1P->bSelfShadowOnly = true;
    Mesh1P->SetCollisionObjectType(ECC_WorldDynamic);
    Mesh1P->SetCollisionEnabled(ECollisionEnabled::NoCollision);
    Mesh1P->SetCollisionResponseToAllChannels(ECR_Ignore);
    Mesh1P->SetupAttachment(RootComponent);

    if (Role == ROLE_Authority) {
        SetReplicates(true);
    }
    
    PrimaryActorTick.bCanEverTick = true;
    PrimaryActorTick.TickGroup = TG_PrePhysics;
    bNetUseOwnerRelevancy = true;

    bIsEquipped = false;
    bWantsToFire = false;
    MuzzleSocketName = FName("Muzzle");

    TimeBetweenShots = 0.5f;
    LastTimeFired = 0.f;
}

void AWeapon::Destroyed() {
    Super::Destroyed();
    GetWorldTimerManager().ClearTimer(TimerHandle_FireWeapon);
}

AFCPlayerController* AWeapon::GetOwningPlayer() const {
    return FCOwner != nullptr ? Cast<AFCPlayerController>(FCOwner->GetController()) : nullptr;
}

USkeletalMeshComponent* AWeapon::GetWeaponMesh() const {
    return (GetOwningPlayer() != nullptr && GetOwningPlayer()->IsLocalPlayerController()) ? Mesh1P : Mesh3P;
}


void AWeapon::SetNewOwner(class AFightclashCharacter* Owner) {
    FCOwner = Owner;
    Instigator = FCOwner;
    SetOwner(FCOwner);
}

void AWeapon::EquipToCharacter(AFightclashCharacter* EquippingCharacter) {
    if (bIsEquipped) {
        UnequipFromOwner();
    }

    SetNewOwner(EquippingCharacter);

    Mesh3P->AttachToComponent(FCOwner->GetMesh3P(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FCOwner->GripSocketName3P);
    Mesh3P->bUseAttachParentBound = true;
    Mesh3P->SetHiddenInGame(false);

    Mesh1P->AttachToComponent(FCOwner->GetMesh1P(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, FCOwner->GripSocketName1P);
    Mesh1P->bUseAttachParentBound = true;
    Mesh1P->SetHiddenInGame(false);

    bIsEquipped = true;
}

void AWeapon::UnequipFromOwner() {
    Mesh3P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
    Mesh3P->SetHiddenInGame(true);

    Mesh1P->DetachFromComponent(FDetachmentTransformRules::KeepRelativeTransform);
    Mesh1P->SetHiddenInGame(true);

    bIsEquipped = false;
}

void AWeapon::StartFire() {
    if (Role < ROLE_Authority) {
        ServerStartFire();
    }

    if (bWantsToFire) {
        return;
    }

    bWantsToFire = true;
    UpdateWeaponState();
}

void AWeapon::ServerStartFire_Implementation() {
    StartFire();
}

bool AWeapon::ServerStartFire_Validate() {
    return true;
}

void AWeapon::StopFire() {
    if (Role < ROLE_Authority) {
        ServerStopFire();
    }

    if (!bWantsToFire) {
        return;
    }

    bWantsToFire = false;
    UpdateWeaponState();
}

void AWeapon::ServerStopFire_Implementation() {
    StopFire();
}

bool AWeapon::ServerStopFire_Validate() {
    return true;
}

void AWeapon::UpdateWeaponState() {
    EWeaponState NextState = EWeaponState::Idle;
    if (bWantsToFire) {
        NextState = EWeaponState::Firing;
    }
    SetWeaponState(NextState);
}

void AWeapon::SetWeaponState(EWeaponState NewState) {
    if (CurrentState == NewState) {
        return;
    }

    if (NewState == EWeaponState::Firing) {
        FireWeapon();
    }
    else if (CurrentState == EWeaponState::Firing) {
        GetWorldTimerManager().ClearTimer(TimerHandle_FireWeapon);
    }

    CurrentState = NewState;
}

void AWeapon::FireWeapon() {

    if (Instigator == nullptr) {
        return;
    }

    // If we can't fire yet, set a timer for when we can and cancel
    // [client] This cancels out before a request to fire is sent to the server
    // [server] If a request is received from the client but the character can't fire yet, assume that we should fire at the next chance
    float CurrentTime = GetWorld()->GetTimeSeconds();
    if (LastTimeFired > 0.f && LastTimeFired + TimeBetweenShots > CurrentTime) {
        GetWorldTimerManager().SetTimer(TimerHandle_FireWeapon, this, &AWeapon::FireWeapon, LastTimeFired + TimeBetweenShots - CurrentTime, false);
        return;
    }

    // If we aren't the server, notify the server and apply any local effects resulting from the shot
    if (Role < ROLE_Authority) {
        FireSingleShotLocal();
    }
    // If we are the server, simulate the actual shot and notify other clients about the effects
    else {
        FireSingleShot();
        FireWeaponEffects();
    }

    // Both server and client should remember the last time a shot was fired
    float PreviousTimeFired = LastTimeFired;
    LastTimeFired = GetWorld()->GetTimeSeconds();
    GetWorldTimerManager().SetTimer(TimerHandle_FireWeapon, this, &AWeapon::FireWeapon, TimeBetweenShots, false);
}

void AWeapon::GetShotOriginAndDirection(FVector& Origin, FVector& Direction) const {
    Origin = GetMuzzleLocation();
    Direction = GetAimDirection();
}

FVector AWeapon::GetAimDirection() const {
    return Instigator->GetControlRotation().Vector();
}

FVector AWeapon::GetMuzzleLocation() const {
    return Mesh1P->GetSocketLocation(MuzzleSocketName);
}

void AWeapon::FireWeaponEffects_Implementation() {
    if (GetNetMode() == ENetMode::NM_DedicatedServer) {
        return;
    }

    // try and play the sound if specified
    if (FireSound != nullptr)
    {
        UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
    }

    // try and play a firing animation if specified
    // First Person
    if (FPFireAnimation != nullptr)
    {
        // Get the animation object for the arms mesh
        UAnimInstance* AnimInstance = FCOwner->GetMesh1P()->GetAnimInstance();
        if (AnimInstance != nullptr)
        {
            AnimInstance->Montage_Play(FPFireAnimation, 1.f);
        }
    }
    // Third Person
    if (TPFireAnimation != nullptr)
    {
        // Get the animation object for the arms mesh
        UAnimInstance* AnimInstance = FCOwner->GetMesh()->GetAnimInstance();
        if (AnimInstance != nullptr)
        {
            AnimInstance->Montage_Play(TPFireAnimation, 1.f);
        }
    }
}
