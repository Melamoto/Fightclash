// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "Weapon_Projectile.generated.h"

/**
 * 
 */
UCLASS(Abstract, Blueprintable, BlueprintType)
class FIGHTCLASH_API AWeapon_Projectile : public AWeapon
{
	GENERATED_BODY()
public:

    virtual void FireSingleShot() override;

    UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Gameplay|Weapon Properties")
    TSubclassOf<class AFightclashProjectile> ProjectileClass;

};
